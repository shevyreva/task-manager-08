package ru.t1.shevyreva.tm;

import ru.t1.shevyreva.tm.constant.ApplicationConst;
import ru.t1.shevyreva.tm.constant.ArgumentConst;
import ru.t1.shevyreva.tm.constant.CommandConst;
import ru.t1.shevyreva.tm.model.Command;
import ru.t1.shevyreva.tm.repository.CommandRepository;
import ru.t1.shevyreva.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("**Welcome to Task Manager**");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        exit();
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command.toLowerCase()) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.INFO:
                showInfo();
                break;
            case CommandConst.COMMANDS:
                showCommand();
                break;
            case CommandConst.ARGUMENTS:
                showArgument();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.COMMANDS:
                showCommand();
                break;
            case ArgumentConst.ARGUMENTS:
                showArgument();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    public static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.printf("This command is not supported. Enter command \"%s\" for command list. \n", CommandConst.HELP);
        exitError();
    }

    public static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.printf("This argument is not supported. Enter argument \"%s\" for command list. \n", ArgumentConst.HELP);
        exitError();
    }

    public static void exit() {
        System.exit(0);
    }

    public static void exitError() {
        System.exit(1);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Shevyreva Liya");
        System.out.println("e-mail: liyavmax@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.printf("%s.%s.%s \n", ApplicationConst.APP_MAJOR_VERSION, ApplicationConst.APP_MINOR_VERSION, ApplicationConst.APP_FIXES_VERSION);
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

    public static void showCommand() {
        System.out.println("[COMMAND]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public static void showArgument() {
        System.out.println("[ARGUMENT]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
