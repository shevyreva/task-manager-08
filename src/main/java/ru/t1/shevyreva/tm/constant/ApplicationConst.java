package ru.t1.shevyreva.tm.constant;

public final class ApplicationConst {

    public static final int APP_MAJOR_VERSION = 1;

    public static final int APP_MINOR_VERSION = 8;

    public static final int APP_FIXES_VERSION = 0;

}
