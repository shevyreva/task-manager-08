package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.constant.ArgumentConst;
import ru.t1.shevyreva.tm.constant.CommandConst;
import ru.t1.shevyreva.tm.model.Command;

public class CommandRepository {

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show about program.");

    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show program version.");

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list.");

    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info.");

    public static final Command COMMANDS = new Command(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show commands.");

    public static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show arguments.");

    public static final Command[] TERMINAL_COMMANDS = new Command[]{ABOUT, HELP, INFO, VERSION, COMMANDS, ARGUMENTS, EXIT};

    public static Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
